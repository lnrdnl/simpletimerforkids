var active = false;
var timer = 0;

var timerElement = document.getElementById('timer');
var timerDoneElement = document.getElementById('timer-done');
var startButton = document.getElementById('start-button');
var resetButton = document.getElementById('reset-button');
var timeToCountSeconds = document.getElementById('time-to-count-seconds');
var timeToCountMinutes = document.getElementById('time-to-count-minutes');

var startTimer = function () {
    active = !active;
    timer = (timeToCountMinutes.value * 60000) + (timeToCountSeconds.value * 1000);
    setTimeout(function () {
        active = false;
        timerDone();
    }, timer);
};

var timerDone = function () {
    active = false;
    timerDoneElement.style.display = "";
    timerElement.style.display = "none"
};



var resetTimer = function () {
    active = false;
    timeToCountMinutes.value = '';
    timeToCountSeconds.value = '';
    timer = 0;
    timerElement.style.width = '0%';
    timeToCountMinutes.focus();
    timerElement.style.display = "";
    timerDoneElement.style.display = "none";
};

var disableInputFields = function () {
    timeToCountMinutes.disabled = true;
    timeToCountSeconds.disabled = true;
};

var enableInputFields = function () {
    timeToCountMinutes.disabled = false;
    timeToCountSeconds.disabled = false;
};

startButton.addEventListener('click', startTimer);
resetButton.addEventListener('click', resetTimer);
timeToCountMinutes.addEventListener('keypress', function (e) {
    var key = e.which || e.keyCode;
    if (key === 13) { // 13 is enter
        startTimer();
    }
});
timeToCountSeconds.addEventListener('keypress', function (e) {
    var key = e.which || e.keyCode;
    if (key === 13) { // 13 is enter
        startTimer();
    }
});

timerElement.style.width = '0%';
timeToCountMinutes.focus();

setInterval(function () {
    if (active) {
        disableInputFields();
        var factor = 100 / (parseFloat(timer) / 1000);
        var currentWith = parseFloat(timerElement.style.width);
        var newWidth = currentWith + factor;
        timerElement.style.width = newWidth + '%';
        console.log(timerElement.style.width);
    } else {
        enableInputFields();
    }
}, 1000);
